package com.autotest.testCase;

import com.autotest.entity.Paramteries;
import com.autotest.utils.HttpClientUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.testng.Assert;
import org.testng.annotations.Test;


import static com.autotest.utils.PoiToExcelUtils.readExcel;

public class TestCase {
    public static Logger logger = Logger.getLogger(TestCase.class);


    public static org.json.JSONObject resultToJson(){

        Paramteries paramteries = new Paramteries();
        String str = HttpClientUtils.Xmltorequest(paramteries.getUrl(),paramteries.getXml());
        org.json.JSONObject jsonObject = XML.toJSONObject(str);
        org.json.JSONObject jsonBody = jsonObject.getJSONObject("service").getJSONObject("Body");
        //System.out.println(jsonBody.toString());

        return jsonBody;
    }

    /*public static void main(String[] args) {
        JSONObject obj = resultToJson();
        System.out.println(obj.toString());
        for (String key: obj.keySet()) {
            System.out.println("key的值："+key+"===>value的值："+obj.get(key));

        }
    }*/

    @Test
    public void test1(){
        JSONObject object = resultToJson();
        JSONObject jsonObject = readExcel("data/testData.xls","expected");

        for (String key: object.keySet()) {
            if ((object.has(key)== jsonObject.has(key))){
                Assert.assertEquals(object.get(key).toString(),jsonObject.get(key).toString());
                System.out.println("key等于"+key+"实际值："+object.get(key)+", 期望值："+jsonObject.get(key));
            }

        }
    }

    @Test
    public void testEcifNo(){
        JSONObject object = resultToJson();
        JSONObject jsonObject = readExcel("data/testData.xls","expected");
        if (object.has("ecifNo")== jsonObject.has("ecifNo")){
            Assert.assertEquals(object.get("ecifNo"),jsonObject.get("ecifNo"));
        }
    }

    @Test
    public void testProduct(){
        JSONObject object = resultToJson();
        JSONObject jsonObject = readExcel("data/testData.xls","expected");
        String  actrul = object.getString("product");
        String except = jsonObject.getString("product");
        Assert.assertEquals(actrul,except);
    }

    @Test
    public void testcontrollerDNo(){
        JSONObject object = resultToJson();
        JSONObject jsonObject = readExcel("data/testData.xls","expected");
        String  actrul = object.getString("controllerDNo");
        Assert.assertEquals(actrul,"6450199250607130");
    }


}
