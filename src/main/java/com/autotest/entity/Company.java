package com.autotest.entity;

public class Company {
    private String ecifNo;
    private String product;
    private String controllerDNo;
    private String controllerDType;
    private String entName;
    private String uniSocailCreCode;
    private String queryId;
    private String custOrgCode;
    private String enterpriseCertType;
    private String controllerName;
    private String coreCustNo;
    private String userNotesid;
    private String enterpriseCerNum;

    public String getEcifNo() {
        return ecifNo;
    }

    public void setEcifNo(String ecifNo) {
        this.ecifNo = ecifNo;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getControllerDNo() {
        return controllerDNo;
    }

    public void setControllerDNo(String controllerDNo) {
        this.controllerDNo = controllerDNo;
    }

    public String getControllerDType() {
        return controllerDType;
    }

    public void setControllerDType(String controllerDType) {
        this.controllerDType = controllerDType;
    }

    public String getEntName() {
        return entName;
    }

    public void setEntName(String entName) {
        this.entName = entName;
    }

    public String getUniSocailCreCode() {
        return uniSocailCreCode;
    }

    public void setUniSocailCreCode(String uniSocailCreCode) {
        this.uniSocailCreCode = uniSocailCreCode;
    }

    public String getQueryId() {
        return queryId;
    }

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }

    public String getCustOrgCode() {
        return custOrgCode;
    }

    public void setCustOrgCode(String custOrgCode) {
        this.custOrgCode = custOrgCode;
    }

    public String getEnterpriseCertType() {
        return enterpriseCertType;
    }

    public void setEnterpriseCertType(String enterpriseCertType) {
        this.enterpriseCertType = enterpriseCertType;
    }

    public String getControllerName() {
        return controllerName;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }

    public String getCoreCustNo() {
        return coreCustNo;
    }

    public void setCoreCustNo(String coreCustNo) {
        this.coreCustNo = coreCustNo;
    }

    public String getUserNotesid() {
        return userNotesid;
    }

    public void setUserNotesid(String userNotesid) {
        this.userNotesid = userNotesid;
    }

    public String getEnterpriseCerNum() {
        return enterpriseCerNum;
    }

    public void setEnterpriseCerNum(String enterpriseCerNum) {
        this.enterpriseCerNum = enterpriseCerNum;
    }

    @Override
    public String toString(){
        return "{\"ecifNo\":" + ecifNo +
                ",\"product\":" + product +
                ",\"controllerDNo\":" +controllerDNo+
                ",\"controllerDType\":" + controllerDType+
                ",\"entName\":" + entName+
                ",\"uniSocailCreCode\":" + uniSocailCreCode+
                ",\"queryId\":" + queryId +
                ",\"custOrgCode\":" + custOrgCode+
                ",\"enterpriseCertType\":" + enterpriseCertType+
                ",\"controllerName\":" + controllerName+
                ",\"coreCustNo\":" + coreCustNo +
                ",\"userNotesid\":" + userNotesid +
                ",\"enterpriseCerNum\":"+enterpriseCerNum+"}";
    }
}
