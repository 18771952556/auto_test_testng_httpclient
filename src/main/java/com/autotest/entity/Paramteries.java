package com.autotest.entity;

import java.util.HashMap;

public class Paramteries {

    /*public static void main(String[] args) {
        HashMap<String,String> headerMap = new HashMap<>();
        headerMap.put("Content-Type","application/xml");
        headerMap.put("x-api-key","PMAK-65976ae3ea82b62a17f4e451-00a80384a3eb769f14b11a24f9121688d4");
    }*/

    private String ContentType="application/xml";
    private String xapikey="PMAK-65976ae3ea82b62a17f4e451-00a80384a3eb769f14b11a24f9121688d4";
    private String url = "https://bbd2651f-8eb4-401e-a4cc-3c70172afe7f.mock.pstmn.io/10.7.33.198:8088/apisubmite";
    private String xml ="<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>\n" +
            "<service>\n" +
            "    <requestIp>10.3.63.86</requestIp>\n" +
            "    <Head>\n" +
            "        <Svcd>30720012</Svcd>\n" +
            "        <SvcScn>30720012</SvcScn>\n" +
            "        <CnITp/>\n" +
            "        <Svcd>30720012</Svcd>\n" +
            "        <Svcd>30720012</Svcd>\n" +
            "        <Svcd>30720012</Svcd>\n" +
            "        <Svcd>30720012</Svcd>\n" +
            "        <Svcd>30720012</Svcd>\n" +
            "        <Svcd>30720012</Svcd>\n" +
            "        <Svcd>30720012</Svcd>\n" +
            "        <Svcd>30720012</Svcd>\n" +
            "        <FileNum/>\n" +
            "        <FileVerfCd/>\n" +
            "        <SvcVerNo/>\n" +
            "        <InstId/>\n" +
            "        <TlrNo/>\n" +
            "        <TmINo/>\n" +
            "    </Head>\n" +
            "    <Body>\n" +
            "        <queryId>12345678</queryId>\n" +
            "        <custOrgCode>011170</custOrgCode>\n" +
            "        <controllerName>周杰伦</controllerName>\n" +
            "        <controllerDNo>6450199250607137</controllerDNo>\n" +
            "        <controllerDType>0</controllerDType>\n" +
            "        <product>国际业务</product>\n" +
            "        <uniSocailCreCode>1159753281000003155</uniSocailCreCode>\n" +
            "        <entName>测试企业</entName>\n" +
            "        <ecifNo>900002072010</ecifNo>\n" +
            "        <coreCustNo>1102959031</coreCustNo>\n" +
            "        <userNotesid>150605</userNotesid>\n" +
            "        <enterpriseCertType>1159753281000003155</enterpriseCertType>\n" +
            "        <enterpriseCerNum>1159753281000003155\n" +
            "        </enterp\n" +
            "    </Body>\n" +
            "</service>";


    public String getContentType() {
        return ContentType;
    }

    public void setContentType(String contentType) {
        ContentType = contentType;
    }

    public String getXapikey() {
        return xapikey;
    }

    public void setXapikey(String xapikey) {
        this.xapikey = xapikey;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }
}
