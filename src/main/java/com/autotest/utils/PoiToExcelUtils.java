package com.autotest.utils;

import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

public class PoiToExcelUtils {

    public static JSONObject readExcel(String path,String sheetName){
        //准备好io流
        FileInputStream fileInputStream = null;
        //准备资源
        File file = new File(path);
        //准备一个jsonobject对象 将解析后的结果存放到对象中
        JSONObject jsonObject = new JSONObject();

        try {
            fileInputStream = new FileInputStream(file);

            Workbook  workbook = WorkbookFactory.create(fileInputStream);

            Sheet  sheet = workbook.getSheet(sheetName);

            //获取行数据
            Row row = sheet.getRow(0);
            //获取列的长度,将列的值装入数组中
            int lastcolum = row.getLastCellNum();

            String[] strings = new  String[lastcolum];
            for (int i = 0; i < lastcolum; i++) {

                Cell cell = row.getCell(i);
                String str = cell.getStringCellValue();
                strings[i] = str;

            }
            //获取行的长度
            int lastRow = sheet.getLastRowNum()+1;
            for (int i = 0; i < lastRow; i++) {
                Row row1 = sheet.getRow(i);
                for (int j = 0; j <=lastcolum; j++) {
                    Cell cell = row1.getCell(j);
                    if (cell!=null){
                        cell.setCellType(CellType.STRING);
                        String celldata = cell.getStringCellValue();

                        jsonObject.put(strings[j],celldata);
                    }

                }

            }
            return jsonObject;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (fileInputStream!=null){
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static String ExceltoString(String path,String sheetName){
        //创建一个hashMap 用于接收解析的结果
        HashMap<String, String> map = new HashMap<>();
        //准备资源文件
        File file = new File(path);
        //准备io流
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
            Workbook wb = WorkbookFactory.create(inputStream);
            Sheet sheet = wb.getSheet(sheetName);
            Row row = sheet.getRow(0);
            int lastCellNum = row.getLastCellNum();
            //准备一个数组用于获取列的字段
            String [] strings = new String[lastCellNum];
            for (int i = 0; i < lastCellNum; i++) {
                Cell cell = row.getCell(i);
                String cellData = cell.getStringCellValue();
                strings[i]=cellData;
            }
            int lastRowNum = sheet.getLastRowNum()+1;
            for (int i = 0; i < lastRowNum; i++) {
                Row row1 = sheet.getRow(i);
                for (int j = 0; j <=lastCellNum; j++) {
                    Cell cell = row1.getCell(j);
                    if (cell!=null) {
                        cell.setCellType(CellType.STRING);
                        String celldata = cell.getStringCellValue();
                        map.put(strings[j], celldata);
                    }
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }if (inputStream!=null){
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map.toString();
    }

    public static void main(String[] args) {
        JSONObject jsonObject = readExcel("data/testData.xls","expected");
        System.out.println(jsonObject.toString());

        //System.out.println(ExceltoString("data/testData.xls","expected"));

    }
}
