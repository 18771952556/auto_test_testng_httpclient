package com.autotest.utils;

import org.apache.poi.ss.usermodel.*;

import javax.swing.text.DateFormatter;
import java.io.*;
import java.text.ParseException;
import java.util.Formatter;

public class testPoiDemo {
    public static void readExcel(String excelPath, String sheetName){
        InputStream in = null;
        try {
            File file = new File(excelPath);
            in = new FileInputStream(file);
            Workbook workbook = WorkbookFactory.create(in);
            Sheet sheet = workbook.getSheet(sheetName);
            Row firstRow = sheet.getRow(0);
            int lastCellNum = firstRow.getLastCellNum();
            String[] titles = new String[lastCellNum];
            for (int i = 0; i < lastCellNum; i++) {
                Cell cell = firstRow.getCell(i);
                String title = cell.getStringCellValue();
                titles[i] = title;
            }
            int lastRowNum = sheet.getLastRowNum();
            for (int i = 1; i <= lastRowNum  ; i++) {
                Row rowData = sheet.getRow(i);
                System.out.print("第"+i+"行数据：");
                for (int j = 0; j < lastCellNum ; j++) {
                    Cell cell = rowData.getCell(j);
                    String cellValue = cell.getStringCellValue();
                    // 打印获取到的值
                    System.out.print("【"+ titles[j] + "="+ cellValue+"】");
                }
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in!=null){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {

        readExcel("data/caseData.xlsx","variables");

        File file = new File("data/caseData.xlsx");

        FileInputStream fileInputStream = null;


        try {
            fileInputStream = new FileInputStream(file);

            Workbook workbook = WorkbookFactory.create(fileInputStream);

            Sheet sheet = workbook.getSheet("scenesCase");
            //获取行数据
            Row row = sheet.getRow(0);
            // 通过行获取列的长度
            int lastcolum = row.getLastCellNum();
            String [] strings = new String[lastcolum];
            for (int i = 0; i < lastcolum; i++) {
                //获取单元格
                Cell cell = row.getCell(i);
                //System.out.println(cell);
                //获取单元格的值
                String string2 = cell.getStringCellValue();
                System.out.println(string2);
                strings[i]= string2;

            }
            //获取行的长度
            int lastrownum = sheet.getLastRowNum()-1;
            System.out.println(lastrownum);
            for (int i = 1; i < lastrownum; i++) {
                //获取行的数据
                Row rowdata = sheet.getRow(i);
                System.out.println("第"+i+"行的数据");
                for (int j = 1; j < lastcolum; j++) {
                    Cell cell = rowdata.getCell(j);
                    if(cell!=null) {
                        cell.setCellType(CellType.STRING);
                        String celldata = cell.getStringCellValue();
                        System.out.println("【" + strings[j] + "=" + celldata + "】");
                    }

                }
                System.out.println();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (fileInputStream!=null){
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
