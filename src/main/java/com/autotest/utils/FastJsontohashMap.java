package com.autotest.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.autotest.entity.User;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class FastJsontohashMap {

    public static void main(String[] args) {
        String paramerters = "{\"userName\":\"autotest\",\"password\":\"123456\"}";

        HashMap<String, String> hashMap = new HashMap<String, String>();

        JSONObject object = JSON.parseObject(paramerters);

        //将jsonobject 转换为map
        Set<String> keys = object.keySet();
        for (String key :keys) {
            hashMap.put(key, object.getString(key));
            System.out.println("key的值："+key+"value的值："+hashMap.get(key));
        }
        //遍历map
        Set<String> username = object.keySet();
        for (String name: username) {
            System.out.println("key的值:"+name+"===>"+"value的值"+hashMap.get(name));

        }
        //将json数组转换为对象集合

        String paramters2 = "[{\"name\":\"autoapi\",\"age\":\"20\"},{\"name\":\"autoui\",\"age\":\"22\"}]";

        List<User> users = JSONObject.parseArray(paramters2,User.class);
        for (User user: users) {
            System.out.println(user.toString());

        }

        //使用jsonpath

        String ref =  "{\"code\":9420, \"msg\":\"恭喜qzcsbj，登录成功\",\"token\":\"538bbaba44be5d3d3856718e6c637d02\"}";
        //JSONObject obj = JSON.parseObject(ref);
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(ref);
        String token = JsonPath.read(document,"$.token");
        System.out.println("token的值是"+token);

        String res2 = "{\"code\":\"0\",\"msg\":\"sucess\"," +
                "\"data\":[{\"username\":\"韧\",\"realname\":\"tester1\",\"sex\":\"1\",\"phone\":\"13800000001\"}," +
                "{\"username\":\"全栈测试笔记\",\"realname\":\"tester2\",\"sex\":\"1\",\"phone\":\"13800000002\"}]}";
        Object documen2 = Configuration.defaultConfiguration().jsonProvider().parse(res2);

        String phone = JsonPath.read(documen2,"$.data[1].phone");

        System.out.println("phone的值是"+phone);

        List<Object> list = JsonPath.read(documen2,"$.data[*].phone");

        System.out.println("list的值===》"+list.toString());

        List<Object> list2 = JsonPath.read(documen2,"$.data[*]['username','realname']");

        System.out.println("list2的值===》"+list2.toString());

        List<HashMap> maps = JSONObject.parseArray(JSONObject.toJSONString(list2),HashMap.class);

        for (HashMap map:maps) {
            if ("韧".equals(map.get("username"))){
                System.out.println("韧的真实姓名"+map.get("realname"));
                break;
            }

        }

    }
}