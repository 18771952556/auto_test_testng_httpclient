package com.autotest.utils;

import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

public class testPoiDemo2 {
    public static void main(String[] args) {
        FileInputStream fileInputStream = null;
        File file = new File("data/caseData.xlsx");
        HashMap<String,String> map = new HashMap<>();
        String [] str = null;
        try {
            fileInputStream = new FileInputStream(file);
            Workbook wb = WorkbookFactory.create(fileInputStream);
            Sheet sheet = wb.getSheet("variables");
            Row row = sheet.getRow(0);
            int lastcolum = row.getLastCellNum();
            String [] strings = new String[lastcolum];
            for (int i = 0; i < lastcolum; i++) {
                Cell cell = row.getCell(i);
                 String celldata = cell.getStringCellValue();
                 strings[i]=celldata;
                System.out.println(celldata);

            }
            int lastRowNum = sheet.getLastRowNum();
            System.out.println(lastRowNum);
            for (int i = 1; i <=lastRowNum ; i++) {
                Row row1 = sheet.getRow(i);
                System.out.println("第"+i+"行的数据");
                for (int j = 0; j < lastcolum; j++) {
                    Cell cell = row1.getCell(j);
                    //设置单元格的格式
                    cell.setCellType(CellType.STRING);
                    String celldata = cell.getStringCellValue();
                    map.put(strings[j],celldata );
                    //System.out.println(strings[j]+"="+celldata);
                    System.out.println(map.toString());

                }

            }
            System.out.println(map.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
