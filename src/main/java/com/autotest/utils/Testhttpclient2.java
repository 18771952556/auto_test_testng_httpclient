package com.autotest.utils;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Testhttpclient2 {
    /*
        *
            创建一个HttpClient对象
            创建一个Request对象
            使用HttpClient来执行Request请求，得到对方的response
            处理response
            关闭HttpClient
        * */
    //参数为json格式的
    public static  String postData(String url,HashMap<String, String> map) {

        //创建httpclient
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //创建响应结果对象
        CloseableHttpResponse response = null;
        //创建httPost请求
        HttpPost httpPost = new HttpPost(url);
        //将响应的结果转换成string类型
        String resultToString = "";
        //添加请求头
        httpPost.setHeader("Cont-Type","application/json");
        //创建参数列表
        ArrayList<NameValuePair> list = new ArrayList<>();
        if (map!=null){
            for (String key: map.keySet()) {
                list.add(new BasicNameValuePair(key, map.get(key)));
            }

        }
        //将参数放到表单中
        try {
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list);
            httpPost.setEntity(entity);
            response = httpClient.execute(httpPost);
            resultToString = EntityUtils.toString(response.getEntity(),"utf-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return resultToString;
    }
    //参数为xml格式的
    public static String post(String url , String xml){
        //创建httpclient
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //创建响应结果对象
        CloseableHttpResponse response = null;
        //创建对象  将结果进行转换为string
        String resulttoString = "";
        //创建一个httpPost
        HttpPost httpPost = new HttpPost(url);
        //设置请求头
        httpPost.setHeader("Cont-Type","application/xml");
        try {
            StringEntity entity = new StringEntity(xml);
            httpPost.setEntity(entity);
            response = httpClient.execute(httpPost);
            resulttoString = EntityUtils.toString(response.getEntity());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resulttoString;
    }

    //创建get请求
    public static String resultToGet(String url,HashMap<String, String>map){
        //创建httpclient对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //创建响应对象
        CloseableHttpResponse response = null ;
        //创建一个String 用于接收转换后响应结果
        String resultToGet = null;
        //创建一个url对象
        try {
            URIBuilder builder = new URIBuilder(url);
            if (map!=null) {
                for (String key : map.keySet()) {
                    builder.addParameter(key, map.get(key));

                }
            }
            URI uri =builder.build();

        //执行
            HttpGet httpGet = new HttpGet(uri);
            response = httpClient.execute(httpGet);
            resultToGet = EntityUtils.toString(response.getEntity());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } if (response!=null){
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultToGet;
    }

    public static String get(String url,HashMap<String, String> map){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String resultString = null;
        try {
            URIBuilder builder = new URIBuilder(url);
            if (map!=null){
                for (String key: map.keySet()) {
                    builder.addParameter(key, map.get(key));

                }
            }
            URI uri = builder.build();
            HttpGet httpGet = new HttpGet(uri);
            response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode()==200){
                resultString = EntityUtils.toString(response.getEntity(),"utf-8");
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } if (response!=null){
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultString;
    }

    public static String doPost(String url, Map<String,String> param){
        //创建Httpclient
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //创建响应结果对象
        CloseableHttpResponse response = null;
        //创建一个string用以返回响应结果处理
        String resultString = null;
        //创建一个request对象
        HttpPost httpPost = new HttpPost(url);
        //创建参数列表
        if (param!=null){
            List<NameValuePair> pairList = new ArrayList<>();
            for (String key: param.keySet()) {
                pairList.add(new BasicNameValuePair(key,param.get(key)));

            }
            //模拟表单
            try {
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(pairList);
                httpPost.setEntity(entity);
                //执行http请求
                response = httpClient.execute(httpPost);
                resultString = EntityUtils.toString(response.getEntity(),"utf-8");
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return resultString;
    }
    public static String doGet(String url,HashMap<String,String> map){
        //创建一个httpclient对象
        CloseableHttpClient client = HttpClients.createDefault();
        //创建响应结果对象
        CloseableHttpResponse response = null;
        //将响应结果置换为string
        String resultString = "";

        try {
            //创建uri
            URIBuilder builder = new URIBuilder(url);
            if (map!=null){
                for (String key: map.keySet()) {
                    builder.addParameter(key,map.get(key));
                }
            }
            URI uri = builder.build();
            //创建一个get request对象
            HttpGet httpGet = new HttpGet(uri);
            //执行请求
            response = client.execute(httpGet);

            //判断返回结果状态是否为200

            if (response.getStatusLine().getStatusCode()==200){

                resultString = EntityUtils.toString(response.getEntity(),"utf-8");
            }

        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }finally {
            if (response!=null){
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return resultString;
    }

    public static String getPost(String url,HashMap<String,String> hashMap){

        //创建httpclient对象

        CloseableHttpClient httpClient = HttpClients.createDefault();

        //创建一个httppost对象

        HttpPost httpPost = new HttpPost(url);

        //创建响应结果对象

        CloseableHttpResponse response = null;

        //创建一个String对象

        String result = "";

        //创建列表用于存放参数
        List<NameValuePair> list = new ArrayList<>();

        for (String key: hashMap.keySet()) {
            list.add(new BasicNameValuePair(key, hashMap.get(key)));

        }
        //将列表存放在表单
        try {
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list);
            httpPost.setEntity(entity);
        //执行post请求并将结果存放
            response = httpClient.execute(httpPost);
            result=EntityUtils.toString(response.getEntity(),"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }


    public static String getGet(String uri,HashMap<String,String> map){

        //创建httpclient
        CloseableHttpClient client = HttpClients.createDefault();
        //创建一个响应结果
        CloseableHttpResponse closeableHttpResponse = null;
        //创建一个String对象
        String result = "";
        //创建一个url
        try {
            URIBuilder uriBuilder = new URIBuilder(uri);
            //操作参数
            if (map!=null){
                for (String key: map.keySet()) {
                    uriBuilder.addParameter(key, map.get(key));

                }
            }

            URI url = uriBuilder.build();
        //创建一个httpget请求对象
            HttpGet httpGet = new HttpGet(url);
        //执行请求
        closeableHttpResponse = client.execute(httpGet);
        if (closeableHttpResponse.getStatusLine().getStatusCode()==200){
            result = EntityUtils.toString(closeableHttpResponse.getEntity(),"utf-8");
        }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (closeableHttpResponse!=null){
                try {
                    closeableHttpResponse.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }



    public static void main(String[] args) {
        String url = "https://api.apiopen.top/api/login";
        HashMap<String,String> hashMap = new HashMap<>();
        String account = "1757056164@qq.com";
        String password = "q1w2e3r4";
        hashMap.put("account",account);
        hashMap.put("password",password);

        String res = doPost(url,hashMap);
        System.out.println(res);

        String result = getPost(url,hashMap);

        System.out.println("响应结果："+result);

        String uri = "https://www.baidu.com/s?ie=utf-8&mod=1&isbd=1&isid=c1a64ce7002cbbf2&ie=utf-8&f=3&rsv_bp=1&rsv_idx=1&tn=baidu&wd=%E9%85%B7%E7%8B%97%E9%9F%B3%E4%B9%90&fenlei=256&oq=%25E9%2585%25B7%25E7%258B%2597%25E9%259F%25B3%25E4%25B9%2590&rsv_pq=c1a64ce7002cbbf2&rsv_t=6cd51b%2BMn1q2KYJISMMC1QyAzCxFClgkT4VODrbVKVC9gXwMytxXsdVQYkY&rqlang=cn&rsv_dl=ts_0&rsv_enter=0&rsv_btype=t&prefixsug=%25E9%2585%25B7%25E7%258B%2597%25E9%259F%25B3%25E4%25B9%2590&rsp=0&bs=%E9%85%B7%E7%8B%97%E9%9F%B3%E4%B9%90&rsv_sid=39712_39731_39779_39704_39796_39683_39662_39679_39841_39905_39819_39909_39935_39936_39933_39940_39939_39931&_ss=1&clist=&hsug=&f4s=1&csor=4&_cr1=42774";
        String UserAgent = "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Mobile Safari/537.36";
        HashMap<String,String> hashMap2 = new HashMap<>();
        hashMap2.put("UserAgent",UserAgent);
        String resultGet = getGet(uri,hashMap2);
        System.out.println("get的请求结果："+resultGet);
    }
}
