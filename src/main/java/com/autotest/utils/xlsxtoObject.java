package com.autotest.utils;

import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class xlsxtoObject {

    public static void readxlsx(String excelPath,String sheetname){
        FileInputStream inputStream =null;
        try {
            File file = new File(excelPath);
            inputStream = new FileInputStream(file);
            Sheet sheet;
            Workbook workbook = WorkbookFactory.create(inputStream);
            sheet = workbook.getSheet(sheetname);
            Row firstRow = sheet.getRow(0);
            //System.out.println(firstRow);
            int lastCellnum = firstRow.getLastCellNum();
            String[] titles = new String[lastCellnum];
            for (int i = 0; i < titles.length; i++) {
                Cell cell = firstRow.createCell(i);
                String title = cell.getStringCellValue();
                titles[i] = title;
            }
            //System.out.println(titles);
            int lastRowNum = sheet.getLastRowNum();
            for (int i = 0; i < lastRowNum; i++) {
                Row rowData = sheet.getRow(i);
                System.out.println("第" + i + "行数据");
                for (int j = 0; j < lastCellnum; j++) {
                    Cell cell = rowData.createCell(j);
                    String cellValue = cell.getStringCellValue();
                    // 打印获取到的值
                    System.out.println("[" + titles[j] + "=" + cellValue + "]");

                }
                System.out.println();

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public static void main(String[] args) {
        readxlsx("D:\\ideaSpace\\autotest\\data\\caseData.xlsx","scenesCase");
    }
}
