package com.autotest.utils;

import com.autotest.entity.Person;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class TestYaml {
    public static Person getData(String path,Person p){
        ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());

        try {
            return objectMapper.readValue(TestYaml.class.getResourceAsStream(path),p.getClass());
        } catch (IOException e) {
            System.out.println("读取异常：" + e);
        }
        return null;
    }

    public static Person readYaml(String src,Person person){
        ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        FileInputStream fileInputStream = null;


        try {
            fileInputStream = new FileInputStream(src);
            person = objectMapper.readValue(fileInputStream,Person.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return person;
    }

    public static void main(String[] args)  {
        /*Person person = new Person();
        person = getData("caseData/person.yaml",person);
        System.out.println(person);

        if (person!=null){
            System.out.println(person.getAge());
            System.out.println(person.getId());
            System.out.println(person.getName());
        }*/
        /*ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        Person person1 = new Person();
        try {
            objectMapper.readValue(TestYaml.class.getResourceAsStream("src/caseData/person.yaml"),person1.getClass());
            System.out.println(objectMapper);
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        ObjectMapper objectMapper2 = new ObjectMapper(new YAMLFactory());
        InputStream input = null;

        try {
            input =  new FileInputStream("src/caseData/person.yaml");
            Person person2 = objectMapper2.readValue(input, Person.class);
            System.out.println(person2.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Person person = new Person();
        person = readYaml("src/caseData/person.yaml",person);
        System.out.println(person.toString());


    }
}
