package com.autotest.utils;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HttpClientUtils {

    public static String requestPost(String url, HashMap<String,String> hashMap){
        //创建httpclient
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //创建响应结果httpresponse
        CloseableHttpResponse httpResponse = null;
        //创建string 用于输出响应结果
        String resultPost = "";
        //创建httpPost
        HttpPost httpPost = new HttpPost(url);
        //httpPost添加请求头
        /*Paramteries paramteries = new Paramteries();
        HashMap<String ,String> headerMap = new HashMap<>();
        headerMap.put("Content-Type",paramteries.getContentType());
        headerMap.put("x-api-key",paramteries.getXapikey());*/
        httpPost.setHeader("Content-Type","application/xml");
        httpPost.setHeader("x-api-key","PMAK-65976ae3ea82b62a17f4e451-00a80384a3eb769f14b11a24f9121688d4");
        //操作参数列表 将参数存放到NameValuePari类型的list中
        List<NameValuePair> list = new ArrayList<>();
        for (String key:hashMap.keySet()) {
            list.add(new BasicNameValuePair(key,hashMap.get(key)));

        }
        //将参数放到表单中
        try {
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list,"utf-8");
            httpPost.setEntity(entity);
        //执行post请求
            httpResponse = httpClient.execute(httpPost);
            resultPost = EntityUtils.toString(httpResponse.getEntity());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultPost;
    }
    public static String Xmltorequest(String url, String xml)  {
        //创建httpclient
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //创建响应结果httpresponse
        CloseableHttpResponse httpResponse = null;
        //创建string 用于输出响应结果
        String resultPost = "";
        //创建httpPost
        HttpPost httpPost = new HttpPost(url);
        //httpPost添加请求头
        httpPost.addHeader("Content-Type","application/xml");
        httpPost.addHeader("x-api-key","PMAK-65976ae3ea82b62a17f4e451-00a80384a3eb769f14b11a24f9121688d4");
        StringEntity entity = new StringEntity(xml,"utf-8");
        httpPost.setEntity(entity);
        try {
            httpResponse = httpClient.execute(httpPost);
            resultPost = EntityUtils.toString(httpResponse.getEntity(),"utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultPost;
    }
}
